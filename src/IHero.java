/**
 * Created by amen on 8/14/17.
 */
public interface IHero {
    public int getHealth();
    public int getStamina();
    public int getDefencePoints();
    public int getAttackPoints();
}
